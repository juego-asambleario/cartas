# El juego asambleario

¿Qué es el juego asambleario?

El juego asambleario es un intento de ludificar las asambleas.
Se trata de un juego de cartas pensado para jugar durante la celebración de una asamblea.


## Historia del juego

1. Las cartas de rol. Para visibilizar las figuras propias de una asamblea.

2. El juego asambleario original (la presentación de las cartas de rol). Para putear una asamblea. https://docs.google.com/document/d/1If2IuKBAL25R1I4aaEOUZ6hb6oLPDyxj63IGh6dzSuE/edit

3. El juego asambleario (a secas). Para facilitar una asamblea.
https://docs.google.com/spreadsheets/d/1vLlX5KwB5E_CIgS7KQNPaStVkW0Vd_3KB77eeQPYA7M/edit?usp=sharing


## Glosario del juego

- Carta de apuesta/permanente: (a desarrollar).
- Carta de acción: una carta que explica una acción para probar y una puntuación por jugarla.
- Interrupción justificada: intervención de uno de los roles con carta propia, para llevar a cabo su función. Lo que se haga en esa interrupción no puede puntuar para el juego.
- Probar una carta de acción: ejecutar la acción que especifica la carta.
- Punto del orden del día (o solamente punto): periodo de la asamblea en la que se está tratando un asunto de los acordados al principio, y que se supone que tendrá su propio apartado en el acta.
- Turno del juego: periodo de la asamblea en el que cada jugador posee las mismas cartas para jugar.
-Turno de palabra/intervención: periodo en el que el derecho de hablar está concedido a una persona.


## Dinámica del juego

Al inicio de la asamblea en la que se haya decidido jugar al juego asambleario se reparten las cartas de rol. Estas cartas se reparten a quienes quieran asumir el rol, o aleatoriamente. Durante la asamblea, levantar una carta de rol es una "interrupción justificada", y solamente puede hacerse para llevar a cabo una función del rol. Si, por ejemplo, alguien se está saltando el turno de palabra, "El Guardián del Turno" debe levantar su carta y poner orden. El “Moderador” podría levantar su carta e inmediatamente todos los participantes de la asamblea sabrán que el que tiene la carta alzada se va a expresar como Moderador, y no a dar una opinión personal sobre el tema que se está hablando. [Esta parte, realmente, es un poco independiente del juego]

Se acuerda el orden del día, los temas a tratar, claramente separados. En principio, cada punto se correspondería a un turno del juego, aunque si salen demasiados turnos del juego se puede elegir que cada turno del juego se corresponda con más de un punto del orden del día.

Se reparten tantas cartas a cada jugador como turnos vaya a tener el juego.

Se empieza a tratar un punto (y por lo tanto, empieza un turno del juego). Durante el turno, cada persona puede probar todas las cartas de acción que quiera y pueda. Por ejemplo, puede estirar las piernas, y hacer una pregunta, y concluir un punto del día resumiendo lo hablado.

La cuestión es que, al acabar el turno del juego (al acabar de tratar un punto -o varios-) solamente se puede jugar una carta de la mano. En el caso de las cartas de acción, ha de ser una carta que se haya probado. Esa carta se dejará sobre la mesa delante del jugador boca arriba. Si el jugador no tiene ninguna carta que jugar, elige una y la deja sobre la mesa boca abajo.


## Como lo uso?

Imprime el fichero con las [cartas](cartas.pdf) generadas.

Si quieres, puedes usar también cartas de roles de la asamblea (moderador, tomador de acta, guardian del turno...): las puedes encontrar en https://docs.google.com/spreadsheets/d/1vLlX5KwB5E_CIgS7KQNPaStVkW0Vd_3KB77eeQPYA7M/edit?usp=sharing


## Como lo personalizo?

Hemos usado la herramienta: http://cardpen.mcdemarco.net/

Carga en ella los ficheros [csv](cartas.csv), [css](cartas.css) y [html](cartas.html), y a partir de ahí ponlo a tu gusto.
